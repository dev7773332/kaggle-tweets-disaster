# Kaggle Tweets disaster
# Natural Language Processing with Disaster Tweets

Twitter has become an important communication channel in times of emergency.
The ubiquitousness of smartphones enables people to announce an emergency they’re observing in real-time. Because of this, more agencies are interested in programatically monitoring Twitter (i.e. disaster relief organizations and news agencies).

But, it’s not always clear whether a person’s words are actually announcing a disaster.
i this case i have implement the model for the ditecting what people are posting in Tweets.The author explicitly uses the word “ABLAZE” but means it metaphorically. This is clear to a human right away, especially with the visual aid. But it’s less clear to a machine.
i have build a machine learning model that predicts which Tweets are about real disasters and which one’s aren’t. I have access to a dataset of 10,000 tweets that were hand classified.


## How to run the code

*Firstly need to signing the google colab

*after signing upload the files in the colab drive or your drive that can be mount with colab . destination is where you want.

*click on the "Kaggle.ipynb" 


#Before run the code 

there is folder name "Dataset" that you have uploaded before.
please change the the path in code that "#load the data" as files name gievn in Dataset folder.

ex - train_df = pd.read_csv('/content/drive/MyDrive/TweetHUB/Dataset/train.csv', usecols=['id', 'text', 'target'])

change these paths to your path correct file name giving, you can get the path right click on the file =====-===== (/content/drive/MyDrive/TweetHUB/Dataset/train.csv)


#make sure you have install these libraries before run the code,

!pip install tensorflow
!pip install scikit-learn
!pip install flask


#Run

you can run the code one by one that colab showing play button or one time all the code can run in Run time tab top on the colab showing. 

#Genarated file download

you can see after the code has run over  . in the out of the  drive Genarated.csv file has available. in that csv you can see the id and what type of the tweets that id belong.


#how to check flask web application
 
after the running code there will be display this one== https://localhost:5000/ and click on  this it will redirect you the web page that has given and it can be used to download and submt the data id that genrated file



============note!!!!!!!=====

in value of genarated file first is id an second one is the value

1 = positive tweets
0 = not positive 




