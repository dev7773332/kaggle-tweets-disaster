document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('dataForm');

    form.addEventListener('submit', async (e) => {
        e.preventDefault();
        const formData = new FormData(form);
        const id = formData.get('id');

        const response = await fetch('/get_text', {
            method: 'POST',
            body: formData
        });

        const data = await response.text();
        document.getElementById('output').innerHTML = `<p>${data}</p>`;
    });
});


